﻿using Hamburgueria.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Login
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtLogin.Text, txtSenha.Text);

                if (funcionario != null)
                {
                    LogandoUsuario.UsuarioLogado = funcionario;

                    frmLogin menu = new frmLogin();
                    menu.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Hambúrgueria Império", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }

        private void lblEsqueciSenha_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Por favor contate o Administrador do Sistema","Hambúrgueria Império",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
             
        }
    }
}
