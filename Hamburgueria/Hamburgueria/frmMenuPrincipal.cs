﻿using Hamburgueria.DB.Diferenciais.Ibm_Voz;
using Hamburgueria.DB.Funcionário;
using Hamburgueria.Telas.Cliente;
using Hamburgueria.Telas.Estoque;
using Hamburgueria.Telas.Financeiro;
using Hamburgueria.Telas.Fornecedor;
using Hamburgueria.Telas.Funcionário;
using Hamburgueria.Telas.Logística;
using Hamburgueria.Telas.Pedido_de_Compra;
using Hamburgueria.Telas.Produto;
using Hamburgueria.Telas.RH;
using Hamburgueria.Telas.Vendas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            VerificarPermissoes();
            toolStripStatusLabel1.Text = LogandoUsuario.UsuarioLogado.Usuario;
            toolStripStatusLabel4.Text = System.DateTime.Now.ToShortTimeString();
            timer1.Stop();
            

        }

        void VerificarPermissoes()
        {
            if (LogandoUsuario.UsuarioLogado.Admin == false)
            {
                if (LogandoUsuario.UsuarioLogado.RH == false)
                {
                    folhaDePagamentoToolStripMenuItem.Enabled = false;
                    cadastrarToolStripMenuItem.Enabled = false;
                    consultarToolStripMenuItem.Enabled = false;
                  
                }

                if (LogandoUsuario.UsuarioLogado.Vendas == false)
                {
                    produtoToolStripMenuItem1.Enabled = false;
                    pedidoToolStripMenuItem1.Enabled = false;
                    pedidoToolStripMenuItem2.Enabled = false;
                 
                }

                if (LogandoUsuario.UsuarioLogado.Logistica == false)
                {
                    estoqueToolStripMenuItem.Enabled = false;
                  
                }

                if (LogandoUsuario.UsuarioLogado.Compras == false)
                {
                    fornecedorToolStripMenuItem.Enabled = false;
                    produtoToolStripMenuItem.Enabled = false;
                    pedidoToolStripMenuItem.Enabled = false;
                   

                }
                if (LogandoUsuario.UsuarioLogado.Financeiro == false)
                {
                    fluxoDeCaixaToolStripMenuItem.Enabled = false;
                    gastosToolStripMenuItem.Enabled = false;
                }

             

                
            }

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }


        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Deseja realmente sair?", "Hambúrgueria Império",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);
   
              if (r == DialogResult.Yes)
              {
                Application.Exit();
              }
            
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarFuncionario funcionario = new CadastrarFuncionario();
            funcionario.Show();
            this.Hide();

        }

        private void cadastrarFolhaDePToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
          

        }

        private void toolStripStatusLabel4_Click(object sender, EventArgs e)
        {


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = System.DateTime.Now.ToShortTimeString();

        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
            this.Hide();

        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void produtoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CadastrarFolhaDePagamento tela = new CadastrarFolhaDePagamento();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarFolhaDePagamento tela = new ConsultarFolhaDePagamento();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmPonto tela = new frmPonto();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CadastrarFornecedor tela = new CadastrarFornecedor();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ConsultarProduto tela = new ConsultarProduto();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            CadastrarPedido tela = new CadastrarPedido();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            ConsultarPedido tela = new ConsultarPedido();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmProduto tela = new frmProduto();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmProdutoConsultar tela = new frmProdutoConsultar();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            CadastrarCliente tela = new CadastrarCliente();
            tela.Show();
            this.Hide();
 
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            this.Hide();

        }

        private void cadastrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Telas.Pedido_de_Venda.CadastrarPedido tela = new Telas.Pedido_de_Venda.CadastrarPedido();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            Telas.Pedido_de_Venda.ConsultarPedido tela = new Telas.Pedido_de_Venda.ConsultarPedido();
            tela.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Estoque tela = new Estoque();
            tela.Show();
            this.Hide();
 
        }

        private void consultarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            FluxoDeCaixa tela = new FluxoDeCaixa();
            tela.Show();
            this.Hide();

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void frmLogin_Load_1(object sender, EventArgs e)
        {

        }

        private void pontoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            ConsultarPonto ponto = new ConsultarPonto();
            ponto.Show();
            this.Hide();
        }

        private void retirarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RetiradaProdutos tela = new RetiradaProdutos();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Gastos tela = new Gastos();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            ConsultarGastos tela = new ConsultarGastos();
            tela.Show();
            this.Hide();
        }

        private void contextMenuStrip6_Opening(object sender, CancelEventArgs e)
        {

        }
        IbmVoiceApi ibmApi = new IbmVoiceApi();
        private void pictureBox7_Click_1(object sender, EventArgs e)
        {
            ibmApi.IniciarOuvir();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            try
            {
                lblProcess.Visible = true;
                string texto = ibmApi.PararOuvir();
                ibmApi.Verificar(texto);
                this.Hide();
                lblProcess.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império");
                lblProcess.Visible = false;
            }
        }

        private void sMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSMS tela = new frmSMS();
            tela.Show();
            this.Hide();
        }
    }
}
