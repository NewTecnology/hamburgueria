﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Produto
{
    class ProdutoVendaBusiness
    {
        public int Salvar(ProdutoVendaDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do produto é obrigatório");
            }
                

            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preço do produto é obrigatório");
            }

            if (dto.Categoria == string.Empty)
            {
                throw new ArgumentException("Categoria do produto é obrigatório");
            }

            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            db.Remover(id);
        }

        public List<ProdutoVendaDTO> Listar()
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            return db.Listar();
        }

        public List<ProdutoVendaDTO> Consultar(string produto)
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            return db.Consultar(produto);
        }

        public void Alterar (ProdutoVendaDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do produto é obrigatório");
            }


            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preço do produto é obrigatório");
            }

            if (dto.Categoria == string.Empty)
            {
                throw new ArgumentException("Categoria do produto é obrigatório");
            }


            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            db.Alterar(dto);
        }
    }
}
