﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Produto
{
    class ProdutoVendaDatabase
    {
        public int Salvar(ProdutoVendaDTO dto)
        {
            string script = @"INSERT into tb_produto_venda (nm_nome_produto, ds_categoria, vl_preco) VALUES (@nm_nome_produto, @ds_categoria, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_produto", dto.Nome));
            parms.Add(new MySqlParameter("ds_categoria", dto.Categoria));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto_venda WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoVendaDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoVendaDTO> lista = new List<ProdutoVendaDTO>();
            while (reader.Read())
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_nome_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Categoria = reader.GetString("ds_categoria");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public List<ProdutoVendaDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto_venda WHERE nm_nome_produto like @nm_nome_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoVendaDTO> lista = new List<ProdutoVendaDTO>();
            while (reader.Read())
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_nome_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Categoria = reader.GetString("ds_categoria");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Alterar(ProdutoVendaDTO dto)
        {
            string script = @"UPDATE tb_produto_venda 
                                     SET nm_nome_produto = @nm_nome_produto, 
                                         ds_categoria = @ds_categoria, 
                                         vl_preco = @vl_preco 
                                   WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_nome_produto", dto.Nome));
            parms.Add(new MySqlParameter("ds_categoria", dto.Categoria));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
