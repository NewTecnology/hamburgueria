﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_nome,dt_data_cadastro,ds_RG,ds_CPF,ds_CNPJ,ds_email,ds_telefone,ds_celular,ds_rua,ds_CEP,ds_bairro,ds_cidade,ds_estado,bt_pessoafisica,bt_pessoajuridica,ds_cliente) VALUES (@nm_nome,@dt_data_cadastro,@ds_RG,@ds_CPF,@ds_CNPJ,@ds_email,@ds_telefone,@ds_celular,@ds_rua,@ds_CEP,@ds_bairro,@ds_cidade,@ds_estado,@bt_pessoafisica,@bt_pessoajuridica,@ds_cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.DataCadastro));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("bt_pessoafisica", dto.PessoaFisica));
            parms.Add(new MySqlParameter("bt_pessoajuridica", dto.PessoaJuridica));
            parms.Add(new MySqlParameter("ds_cliente", dto.Descricao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }




        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<ClienteDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.DataCadastro = reader.GetDateTime("dt_data_cadastro");
                dto.RG = reader.GetString("ds_RG");
                dto.CPF = reader.GetString("ds_CPF");
                dto.CNPJ = reader.GetString("ds_CNPJ");
                dto.Email = reader.GetString("ds_email");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.Rua = reader.GetString("ds_rua");
                dto.CEP = reader.GetString("ds_CEP");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.PessoaFisica = reader.GetBoolean("bt_pessoafisica");
                dto.PessoaJuridica = reader.GetBoolean("bt_pessoajuridica");
                dto.Descricao = reader.GetString("ds_cliente");



                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }


        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente
                                 SET nm_nome = @nm_nome,
                                     dt_data_cadastro = @dt_data_cadastro,
                                     ds_RG = @ds_RG,
                                     ds_CPF = @ds_CPF,
                                     ds_CNPJ = @ds_CNPJ,
                                     ds_email = @ds_email, 
                                     ds_telefone = @ds_telefone,
                                     ds_celular = @ds_celular,
                                     ds_rua = @ds_rua,
                                     ds_CEP = @ds_CEP,
                                     ds_bairro = @ds_bairro,
                                     ds_cidade = @ds_cidade,
                                     ds_estado = @ds_estado,
                                     bt_pessoafisica = @bt_pessoafisica,
                                     bt_pessoajuridica = @bt_pessoajuridica,
                                     ds_cliente = @ds_cliente
                                     WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.DataCadastro));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("bt_pessoafisica", dto.PessoaFisica));
            parms.Add(new MySqlParameter("bt_pessoajuridica", dto.PessoaJuridica));
            parms.Add(new MySqlParameter("ds_cliente", dto.Descricao));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.DataCadastro = reader.GetDateTime("dt_data_cadastro");
                dto.RG = reader.GetString("ds_RG");
                dto.CPF = reader.GetString("ds_CPF");
                dto.CNPJ = reader.GetString("ds_CNPJ");
                dto.Email = reader.GetString("ds_email");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.Rua = reader.GetString("ds_rua");
                dto.CEP = reader.GetString("ds_CEP");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.PessoaFisica = reader.GetBoolean("bt_pessoafisica");
                dto.PessoaJuridica = reader.GetBoolean("bt_pessoajuridica");
                dto.Descricao = reader.GetString("ds_cliente");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
