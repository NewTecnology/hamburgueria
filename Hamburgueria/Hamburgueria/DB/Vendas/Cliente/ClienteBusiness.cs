﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do cliente é obrigatório");

            }

            if (dto.Rua == string.Empty)
            {
                throw new ArgumentException("Rua é obrigatório");

            }

            if (dto.Bairro == string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório");

            }

            if (dto.Cidade == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório");

            }

            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório");

            }

            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }
        public void Remover(int id)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(id);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }

        public List<ClienteDTO> Consultar(string Cliente)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar(Cliente);
        }

        public void Alterar(ClienteDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do cliente é obrigatório");

            }

            if (dto.Rua == string.Empty)
            {
                throw new ArgumentException("Rua é obrigatório");

            }

            if (dto.Bairro == string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório");

            }

            if (dto.Cidade == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório");

            }

            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório");

            }

            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(dto);
        }
    }
}
