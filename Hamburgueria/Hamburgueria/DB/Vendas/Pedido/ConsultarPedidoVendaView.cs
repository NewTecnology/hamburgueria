﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Pedido
{
    public class ConsultarPedidoVendaView
    {
        public int Id { get; set; }
        public string Cliente { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
