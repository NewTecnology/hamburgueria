﻿using Hamburgueria.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Pedido
{
    class PedidoVendaBusiness
    {
        public int Salvar(PedidoVendaDTO pedido, List<ProdutoVendaDTO> produtos)
        {
            PedidoVendaDatabase pedidoDatabase = new PedidoVendaDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemVendaBusiness itemBusiness = new PedidoItemVendaBusiness();
            foreach (ProdutoVendaDTO item in produtos)
            {
                PedidoItemVendaDTO itemDto = new PedidoItemVendaDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public List<ConsultarPedidoVendaView> Consultar(DateTime inicio, DateTime fim)
        {
            PedidoVendaDatabase db = new PedidoVendaDatabase();
            return db.Consultar(inicio, fim);

        }

        public void Remover(int pedidoId)
        {
            PedidoItemVendaBusiness itemBusiness = new PedidoItemVendaBusiness();
            List<PedidoItemVendaDTO> itens = itemBusiness.ConsultarPorPedido(pedidoId);

            foreach (PedidoItemVendaDTO item in itens)
            {
                itemBusiness.Remover(item.Id);
            }

            PedidoVendaDatabase pedidoDatabase = new PedidoVendaDatabase();
            pedidoDatabase.Remover(pedidoId);

        }

        public void Alterar(PedidoVendaDTO dto)
        {
            PedidoVendaDatabase db = new PedidoVendaDatabase();
            db.Alterar(dto);

        }
    }
}
