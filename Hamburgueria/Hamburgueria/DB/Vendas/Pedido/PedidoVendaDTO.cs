﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Pedido
{
    class PedidoVendaDTO
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Quantidade { get; set; }
        public int IdCliente { get; set; }

    }
}
