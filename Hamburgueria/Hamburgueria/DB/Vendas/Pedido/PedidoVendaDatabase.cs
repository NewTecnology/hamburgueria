﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Pedido
{
    class PedidoVendaDatabase
    {
        public int Salvar(PedidoVendaDTO dto)
        {

            string script = @"INSERT INTO tb_Pedido_Venda (dt_venda,fk_cliente) VALUES (@dt_venda,@fk_cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_venda", dto.Data));
            parms.Add(new MySqlParameter("fk_cliente", dto.IdCliente));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Pedido_Venda WHERE idPedidoVenda = @idPedidoVenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idPedidoVenda", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }



        public void Alterar(PedidoVendaDTO dto)
        {
            string script =
            @"UPDATE tb_Pedido_Venda
                 SET dt_venda = @dt_venda,
               WHERE idPedidoVenda = @idPedidoVenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idPedidoVenda", dto.Id));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }


        public List<ConsultarPedidoVendaView> Consultar(DateTime inicio, DateTime fim)
        {
            string script = @"SELECT * FROM vw_pedido_venda_consultar
                                      Where dt_venda >= @inicio and dt_venda <= @fim";
            ;

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("inicio", inicio));
            parms.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ConsultarPedidoVendaView> lista = new List<ConsultarPedidoVendaView>();
            while (reader.Read())
            {
                ConsultarPedidoVendaView dto = new ConsultarPedidoVendaView();
                dto.Id = reader.GetInt32("idPedidoVenda");
                dto.Cliente = reader.GetString("nm_nome");
                dto.Quantidade = reader.GetInt32("qtd_itens");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.ValorTotal = reader.GetDecimal("vl_total");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
