﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Vendas.Pedido
{
    class PedidoItemVendaBusiness
    {
        public int Salvar(PedidoItemVendaDTO dto)
        {
            PedidoItemVendaDatabase db = new PedidoItemVendaDatabase();
            return db.Salvar(dto);
        }

        public List<PedidoItemVendaDTO> ConsultarPorPedido(int idPedidoVenda)
        {
            PedidoItemVendaDatabase db = new PedidoItemVendaDatabase();
            return db.ConsultarPorPedido(idPedidoVenda);
        }
        public void Remover(int id)
        {
            PedidoItemVendaDatabase db = new PedidoItemVendaDatabase();
            db.Remover(id);
        }
    }
}
