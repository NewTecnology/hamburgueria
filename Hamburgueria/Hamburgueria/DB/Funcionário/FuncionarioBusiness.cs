﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Funcionário
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            if (dto.NomeFuncionario == string.Empty)
            {
                throw new ArgumentException("Nome do funcionario é obrigatório");

            }

            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");

            }

            if (dto.Salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório");

            }

            if (dto.Usuario == string.Empty)
            {
                throw new ArgumentException("Nome do usuário é obrigatório");

            }

            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");

            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);

        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar(nome);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();

        }

        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);

        }

        public void Alterar(FuncionarioDTO dto)
        {
            if (dto.NomeFuncionario == string.Empty)
            {
                throw new ArgumentException("Nome do funcionario é obrigatório");

            }

            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");

            }

            if (dto.Salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório");

            }

            if (dto.Usuario == string.Empty)
            {
                throw new ArgumentException("Nome do usuário é obrigatório");

            }

            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");

            }


            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);

        }


        public FuncionarioDTO Logar(string login, string senha)
        {
            if (login == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(login, senha);
        }

    }
}
