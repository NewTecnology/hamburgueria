﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Funcionário
{
    class FuncionarioDatabase
    {

        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO tb_funcionario (nm_funcionario,ds_endereco,ds_RG,ds_CPF,ds_usuario,ds_senha,vl_salario,vl_vt,vl_va,vl_vr,vl_convenio,bt_admin,bt_funcionario,bt_permissao_RH,bt_permissao_vendas,bt_permissao_compras,bt_permissao_financeiro,bt_permissao_logistica) 
                              VALUES(@nm_funcionario,@ds_endereco,@ds_RG,@ds_CPF,@ds_usuario,@ds_senha,@vl_salario,@vl_vt,@vl_va,@vl_vr,@vl_convenio,@bt_admin,@bt_funcionario,@bt_permissao_RH,@bt_permissao_vendas,@bt_permissao_compras,@bt_permissao_financeiro,@bt_permissao_logistica)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario",dto.NomeFuncionario));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("vl_vt", dto.ValeTransporte));
            parms.Add(new MySqlParameter("vl_va", dto.ValeAlimentacao));
            parms.Add(new MySqlParameter("vl_vr", dto.ValeRefeicao));
            parms.Add(new MySqlParameter("vl_convenio", dto.Convenio));
            parms.Add(new MySqlParameter("bt_admin", dto.Admin));
            parms.Add(new MySqlParameter("bt_funcionario", dto.Funcionario));
            parms.Add(new MySqlParameter("bt_permissao_RH", dto.RH));
            parms.Add(new MySqlParameter("bt_permissao_vendas", dto.Vendas));
            parms.Add(new MySqlParameter("bt_permissao_compras", dto.Compras));
            parms.Add(new MySqlParameter("bt_permissao_financeiro", dto.Financeiro));
            parms.Add(new MySqlParameter("bt_permissao_logistica", dto.Logistica));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<FuncionarioDTO> Listar()
        {
            string script = "SELECT * FROM tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.NomeFuncionario = reader.GetString("nm_funcionario");
                funcionario.Endereco = reader.GetString("ds_endereco");
                funcionario.RG = reader.GetString("ds_rg");
                funcionario.CPF = reader.GetString("ds_cpf");
                funcionario.Usuario = reader.GetString("ds_usuario");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.Salario = reader.GetDecimal("vl_salario");
                funcionario.ValeAlimentacao = reader.GetDecimal("vl_va");
                funcionario.ValeRefeicao = reader.GetDecimal("vl_vr");
                funcionario.ValeTransporte = reader.GetDecimal("vl_vt");
                funcionario.Convenio = reader.GetDecimal("vl_convenio");
                funcionario.Admin = reader.GetBoolean("bt_admin");
                funcionario.Funcionario = reader.GetBoolean("bt_funcionario");
                funcionario.RH = reader.GetBoolean("bt_permissao_RH");
                funcionario.Vendas = reader.GetBoolean("bt_permissao_vendas");
                funcionario.Compras = reader.GetBoolean("bt_permissao_compras");
                funcionario.Financeiro = reader.GetBoolean("bt_permissao_financeiro");
                funcionario.Logistica = reader.GetBoolean("bt_permissao_logistica");

                lista.Add(funcionario);

            }
            reader.Close();

            return lista;

        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_funcionario like @nm_funcionario
                                                             AND nm_funcionario !='Admin'";
                                                             

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.NomeFuncionario = reader.GetString("nm_funcionario");
                funcionario.Endereco = reader.GetString("ds_endereco");
                funcionario.RG = reader.GetString("ds_rg");
                funcionario.CPF = reader.GetString("ds_cpf");
                funcionario.Usuario = reader.GetString("ds_usuario");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.Salario = reader.GetDecimal("vl_salario");
                funcionario.ValeAlimentacao = reader.GetDecimal("vl_va");
                funcionario.ValeRefeicao = reader.GetDecimal("vl_vr");
                funcionario.ValeTransporte = reader.GetDecimal("vl_vt");
                funcionario.Convenio = reader.GetDecimal("vl_convenio");
                funcionario.Admin = reader.GetBoolean("bt_admin");
                funcionario.Funcionario = reader.GetBoolean("bt_funcionario");
                funcionario.RH = reader.GetBoolean("bt_permissao_RH");
                funcionario.Vendas = reader.GetBoolean("bt_permissao_vendas");
                funcionario.Compras = reader.GetBoolean("bt_permissao_compras");
                funcionario.Financeiro = reader.GetBoolean("bt_permissao_financeiro");
                funcionario.Logistica = reader.GetBoolean("bt_permissao_logistica");

                lista.Add(funcionario);

            }
            reader.Close();

            return lista;

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script =
            @"UPDATE tb_funcionario
                 SET nm_funcionario = @nm_funcionario,
	                 ds_CPF        = @ds_CPF,
                     ds_RG        = @ds_RG,
                     ds_endereco   = @ds_endereco,
                     vl_salario    = @vl_salario,
                     vl_vt         = @vl_vt,
                     vl_va         = @vl_va,
                     vl_vr         = @vl_vr,
                     vl_convenio   = @vl_convenio,
                     ds_usuario    = @ds_usuario,
                     ds_senha      = @ds_senha,
                     bt_admin      = @bt_admin,
                     bt_funcionario = @bt_funcionario,
                     bt_permissao_RH  = @bt_permissao_RH,
                     bt_permissao_vendas = @bt_permissao_vendas,
                     bt_permissao_compras = @bt_permissao_compras,
                     bt_permissao_financeiro = @bt_permissao_financeiro,
                     bt_permissao_logistica  = @bt_permissao_logistica 
               WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.NomeFuncionario));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("vl_vt", dto.ValeTransporte));
            parms.Add(new MySqlParameter("vl_va", dto.ValeAlimentacao));
            parms.Add(new MySqlParameter("vl_vr", dto.ValeRefeicao));
            parms.Add(new MySqlParameter("vl_convenio", dto.Convenio));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("bt_admin", dto.Admin));
            parms.Add(new MySqlParameter("bt_funcionario", dto.Funcionario));
            parms.Add(new MySqlParameter("bt_permissao_RH", dto.RH));
            parms.Add(new MySqlParameter("bt_permissao_vendas", dto.Vendas));
            parms.Add(new MySqlParameter("bt_permissao_compras", dto.Compras));
            parms.Add(new MySqlParameter("bt_permissao_financeiro", dto.Financeiro));
            parms.Add(new MySqlParameter("bt_permissao_logistica", dto.Logistica));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }



        public FuncionarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE ds_usuario = @ds_usuario AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_usuario", login));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.NomeFuncionario = reader.GetString("nm_funcionario");
                funcionario.Endereco = reader.GetString("ds_endereco");
                funcionario.RG = reader.GetString("ds_rg");
                funcionario.CPF = reader.GetString("ds_cpf");
                funcionario.Usuario = reader.GetString("ds_usuario");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.Salario = reader.GetDecimal("vl_salario");
                funcionario.ValeAlimentacao = reader.GetDecimal("vl_va");
                funcionario.ValeRefeicao = reader.GetDecimal("vl_vr");
                funcionario.ValeTransporte = reader.GetDecimal("vl_vt");
                funcionario.Convenio = reader.GetDecimal("vl_convenio");
                funcionario.Admin = reader.GetBoolean("bt_admin");
                funcionario.Funcionario = reader.GetBoolean("bt_funcionario");
                funcionario.RH = reader.GetBoolean("bt_permissao_RH");
                funcionario.Vendas = reader.GetBoolean("bt_permissao_vendas");
                funcionario.Compras = reader.GetBoolean("bt_permissao_compras");
                funcionario.Financeiro = reader.GetBoolean("bt_permissao_financeiro");
                funcionario.Logistica = reader.GetBoolean("bt_permissao_logistica");
  
            }
            reader.Close();

            return funcionario;
        }
    }
}
