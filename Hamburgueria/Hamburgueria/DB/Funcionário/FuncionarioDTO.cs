﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Funcionário
{
   public class FuncionarioDTO
    {
        public int Id { get; set; }
        public string NomeFuncionario { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public string Endereco { get; set; }
        public decimal Salario { get; set; }
        public decimal ValeTransporte { get; set; }
        public decimal ValeRefeicao { get; set; }
        public decimal ValeAlimentacao { get; set; }
        public decimal Convenio { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public bool  Admin { get; set; }
        public bool Funcionario { get; set; }
        public bool RH { get; set; }
        public bool Compras { get; set; }
        public bool Vendas { get; set; }
        public bool Logistica { get; set; }
        public bool Financeiro { get; set; }



    }
}
