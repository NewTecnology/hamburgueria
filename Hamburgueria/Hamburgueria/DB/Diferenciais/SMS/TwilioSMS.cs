﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Diferenciais.SMS
{
    class TwilioSMS
    {
        // Acesse https://www.twilio.com/, para criar uma conta e receber as credenciais

        string url = "https://api.twilio.com/2010-04-01/Accounts/AC9610e818845ad7d39a97f3be230a00db/Messages.json";
        string authorization = "AC9610e818845ad7d39a97f3be230a00db:67bb4bade183d1bc572981a4554be230";
        string fromNumber = "+15803243426";


        public async void Enviar(string celular, string mensagem)
        {
            // Prepara cliente para conectar na Api
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(authorization)));

            // Monta parâmetros da chamada Api
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("From", fromNumber);
            parms.Add("To", celular);
            parms.Add("Body", mensagem);

            FormUrlEncodedContent content = new FormUrlEncodedContent(parms);

            // Realiza chamada POST
            var response = await client.PostAsync(url, content);

        }
    }
}