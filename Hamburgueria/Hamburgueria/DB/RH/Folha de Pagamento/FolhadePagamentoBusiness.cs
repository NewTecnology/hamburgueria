﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.RH.Folha_de_Pagamento
{
   public class FolhadePagamentoBusiness
    {
        public int Salvar(FolhadePagamentoDTO dto)
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Salvar(dto);
        }

        public List<FolhadePagamentoView> Consultar(string nome)
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Consultar(nome);
        }

        public List<FolhadePagamentoView> Listar()
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Listar();
        }

        public void Remover(int id)
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            db.Remover(id);
        }

    }
}
