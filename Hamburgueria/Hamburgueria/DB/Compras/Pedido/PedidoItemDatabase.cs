﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Pedido
{
    class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT into tb_compra_item (fk_produto_compra,fk_pedido_compra) VALUES(@fk_produto_compra,@fk_pedido_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto_compra", dto.IdProduto));
            parms.Add(new MySqlParameter("fk_pedido_compra", dto.IdPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<PedidoItemDTO> ConsultarPorPedido(int idPedido)
        {
            string script = @"SELECT * FROM tb_compra_item WHERE fk_pedido_compra = @fk_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_pedido_compra", idPedido));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoItemDTO> lista = new List<PedidoItemDTO>();
            while (reader.Read())
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.Id = reader.GetInt32("id_compra_item");
                dto.IdPedido = reader.GetInt32("fk_pedido_compra");
                dto.IdProduto = reader.GetInt32("fk_produto_compra");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<PedidoItemDTO> ConsultarPorId(int idCompra)
        {
            string script = @"SELECT * FROM tb_compra_item
                                      Where fk_produto_compra = @fk_produto_compra";
            ;

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto_compra", idCompra));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoItemDTO> lista = new List<PedidoItemDTO>();
            while (reader.Read())
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.Id = reader.GetInt32("id_compra_item");
                dto.IdPedido = reader.GetInt32("fk_pedido_compra");
                dto.IdProduto = reader.GetInt32("fk_produto_compra");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_compra_item WHERE id_compra_item = @id_compra_item";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra_item", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
