﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Pedido
{
   public class PedidoItemDTO
   {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int IdProduto { get; set; }

    }
}
