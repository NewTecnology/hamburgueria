﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Pedido
{
    class PedidoItemBusiness
    {
        public int Salvar(PedidoItemDTO dto)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            return db.Salvar(dto);
        }

        public List<PedidoItemDTO> ConsultarPorPedido(int idPedido)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            return db.ConsultarPorPedido(idPedido);
        }

        public List<PedidoItemDTO> ConsultarPorId(int IdCompra)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            return db.ConsultarPorId(IdCompra);
        }
        public void Remover(int id)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            db.Remover(id);
        }
    }
}
