﻿using Hamburgueria.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoConsultarView> produtos)
        {

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoConsultarView item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }
        public List<PedidoCompraConsultarView> Consultar(DateTime inicio, DateTime fim)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Consultar(inicio, fim);

        }
        public List<PedidoCompraConsultarView> ConsultarPorId(int idCompra)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.ConsultarPorId(idCompra);

        }

        public void Remover(int pedidoId)
        {
            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            List<PedidoItemDTO> itens = itemBusiness.ConsultarPorPedido(pedidoId);

            foreach (PedidoItemDTO item in itens)
            {
                itemBusiness.Remover(item.Id);
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            pedidoDatabase.Remover(pedidoId);

        }

        public void Alterar(PedidoDTO dto)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Alterar(dto);

        }



    }
}
