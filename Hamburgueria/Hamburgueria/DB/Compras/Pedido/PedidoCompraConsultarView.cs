﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Pedido
{
   public class PedidoCompraConsultarView
   {
        public int Id { get; set; }
        public string Fornecedor { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }
        public decimal ValorTotal { get; set; }
        public int IdProduto { get; set; }
        
   }
}
