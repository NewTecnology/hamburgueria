﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.DB.Compras.Fornecedor
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do fornecedor é obrigatório");

            }

            if (dto.Rua == string.Empty)
            {
                throw new ArgumentException("Rua é obrigatório");

            }

            if (dto.Cidade == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório");

            }

            if (dto.Bairro == string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório");

            }

            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório");

            }

            FornecedorDatabase db = new FornecedorDatabase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Remover(id);
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();
        }

        public List<FornecedorDTO> Consultar(String Fornecedor)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Consultar(Fornecedor);
        }

        public void Alterar(FornecedorDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do fornecedor é obrigatório");

            }

            if (dto.Rua == string.Empty)
            {
                throw new ArgumentException("Rua é obrigatório");

            }

            if (dto.Cidade == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório");

            }

            if (dto.Bairro == string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório");

            }

            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório");

            }

            FornecedorDatabase db = new FornecedorDatabase();
            db.Alterar(dto);
        }
    }
}