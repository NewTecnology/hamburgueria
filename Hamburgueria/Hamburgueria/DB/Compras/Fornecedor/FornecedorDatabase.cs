﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Fornecedor
{
    class FornecedorDatabase
    {
        public List<FornecedorDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_fornecedor WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_nome");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.CNPJ = reader.GetString("ds_CNPJ");
                dto.Email = reader.GetString("ds_email");
                dto.CEP = reader.GetString("ds_CEP");
                dto.Rua = reader.GetString("ds_rua");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Estado = reader.GetString("ds_estado");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_nome");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.CNPJ = reader.GetString("ds_CNPJ");
                dto.Email = reader.GetString("ds_email");
                dto.CEP = reader.GetString("ds_CEP");
                dto.Rua = reader.GetString("ds_rua");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Estado = reader.GetString("ds_estado");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor
                                 SET nm_nome = @nm_nome,
                                     ds_rua = @ds_rua,
                                     ds_CEP = @ds_CEP,
                                     ds_email = @ds_email,
                                     ds_cidade = @ds_cidade,
                                     ds_estado = @ds_estado,
                                     ds_telefone = @ds_telefone,
                                     ds_CNPJ = @ds_CNPJ,
                                     ds_bairro = @ds_bairro
                     WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor (id_fornecedor, nm_nome, ds_telefone, ds_email, ds_CNPJ, ds_rua, ds_CEP, ds_bairro, ds_cidade, ds_estado) VALUES (@id_fornecedor, @nm_nome, @ds_telefone, @ds_email, @ds_CNPJ, @ds_rua, @ds_CEP, @ds_bairro, @ds_cidade, @ds_estado)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


    }
}
