﻿using Hamburgueria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO pdt)
        {
            string script = @"INSERT INTO tb_produto_compra (nm_nome_produto,vl_preco,ds_marca,fk_id_fornecedor) 
                            VALUES (@nm_nome_produto,@vl_preco,@ds_marca,@fk_id_fornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_produto", pdt.Nome));
            parms.Add(new MySqlParameter("vl_preco", pdt.Preco));
            parms.Add(new MySqlParameter("ds_marca", pdt.Marca));
            parms.Add(new MySqlParameter("fk_id_fornecedor", pdt.IdFornecedor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto_compra WHERE id_produto like @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<ProdutoConsultarView> Listar()
        {
            string script = @"SELECT * FROM vw_consultar_produto_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultarView> lista = new List<ProdutoConsultarView>();
            while (reader.Read())
            {
                ProdutoConsultarView dto = new ProdutoConsultarView();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_nome_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Marca = reader.GetString("ds_marca");
                dto.Fornecedor = reader.GetString("nm_nome");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }

        public List<ProdutoConsultarView> Consultar(string produto)
        {
            string script = @"SELECT * FROM vw_consultar_produto_compra WHERE nm_nome_produto like @nm_nome_produto";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultarView> lista = new List<ProdutoConsultarView>();
            while (reader.Read())
            {
                ProdutoConsultarView dto = new ProdutoConsultarView();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_nome_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Marca = reader.GetString("ds_marca");
                dto.Fornecedor = reader.GetString("nm_nome");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoConsultarView> ConsultarPorId(int idProduto)
        {
            string script = @"SELECT * FROM vw_consultar_produto_compra WHERE id_produto like @id_produto";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", idProduto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultarView> lista = new List<ProdutoConsultarView>();
            while (reader.Read())
            {
                ProdutoConsultarView dto = new ProdutoConsultarView();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_nome_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Marca = reader.GetString("ds_marca");
                dto.Fornecedor = reader.GetString("nm_nome");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }



        public List<ProdutoConsultarView> ConsultarPorFornecedor(string fornecedor)
        {
            string script = @"SELECT * FROM vw_consultar_produto_compra WHERE nm_nome like @nm_nome";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", fornecedor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultarView> lista = new List<ProdutoConsultarView>();
            while (reader.Read())
            {
                ProdutoConsultarView dto = new ProdutoConsultarView();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_nome_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Marca = reader.GetString("ds_marca");
                dto.Fornecedor = reader.GetString("nm_nome");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script =
            @"UPDATE tb_produto_compra
                 SET nm_nome_produto = @nm_nome_produto,
	                 vl_preco = @vl_preco,
                     ds_marca = @ds_marca,
                     fk_id_fornecedor = @fk_id_fornecedor
               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_nome_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("ds_marca", dto.Marca));
            parms.Add(new MySqlParameter("fk_id_fornecedor", dto.IdFornecedor));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
    }

}










