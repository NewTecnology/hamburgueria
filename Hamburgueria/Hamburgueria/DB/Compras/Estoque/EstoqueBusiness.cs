﻿using Hamburgueria.DB.Compras.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Compras.Produto
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Salvar(dto);

        }

        public List<EstoqueConsultarView> Consultar(string produto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Consultar(produto);

        }

        public List<EstoqueConsultarView> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }

        public List<EstoqueDTO> Listar2()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar2();
        }


        public void Alterar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Remover(id);

        }
    }
}
