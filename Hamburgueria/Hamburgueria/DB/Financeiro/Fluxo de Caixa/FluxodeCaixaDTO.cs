﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Financeiro.Fluxo_de_Caixa
{
    public class FluxodeCaixaDTO
    {
        public DateTime DataReferencia { get; set; }
        public decimal ValorGanhos { get; set; }
        public decimal ValorDespesas { get; set; }
        public decimal ValorLucros { get; set; }
    }
}
