﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamburgueria.DB.Financeiro.Fluxo_de_Caixa
{
    class FluxodeCaixaBusiness
    {
        public List<FluxodeCaixaDTO> Consultar(DateTime inicio, DateTime fim)
        {
            FluxodeCaixaDatabase db = new FluxodeCaixaDatabase();
            return db.Consultar(inicio, fim);

        }
    }
}
