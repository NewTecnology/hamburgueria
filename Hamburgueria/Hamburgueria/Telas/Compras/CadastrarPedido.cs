﻿using Hamburgueria.DB.Compras.Estoque;
using Hamburgueria.DB.Compras.Fornecedor;
using Hamburgueria.DB.Compras.Pedido;
using Hamburgueria.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Pedido_de_Compra
{
    public partial class CadastrarPedido : Form
    {
        BindingList<ProdutoConsultarView> produtosCarrinho = new BindingList<ProdutoConsultarView>();
        public CadastrarPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;
        }
        void ConfigurarGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = produtosCarrinho;
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoConsultarView dto = cboProduto.SelectedItem as ProdutoConsultarView;

            int qtd = Convert.ToInt32(numericUpDown1.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto2 = comboBox1.SelectedItem as FornecedorDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.IdFornecedor = dto2.Id;
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                int idPedido = business.Salvar(dto, produtosCarrinho.ToList());

                EstoqueBusiness business2 = new EstoqueBusiness();
                List<PedidoCompraConsultarView> lista = business.ConsultarPorId(idPedido);
                List<EstoqueDTO> estoque = business2.Listar2();

                foreach (PedidoCompraConsultarView item in lista)
                {
                    foreach (EstoqueDTO item2 in estoque)
                    {
                        if(item.IdProduto == item2.IdProduto)
                        {
                            item2.Quantidade = item2.Quantidade + item.Quantidade;
                        }
                    }
                }

                foreach (EstoqueDTO item in estoque)
                {
                    EstoqueDTO estoquedto = new EstoqueDTO();

                    estoquedto.Id = item.Id;
                    estoquedto.IdProduto = item.IdProduto;
                    estoquedto.Quantidade = item.Quantidade;

                    business2.Alterar(estoquedto);
                }



                MessageBox.Show("Pedido Salvo com sucesso", "Hambúrgueria Império", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                  MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO dto = comboBox1.SelectedItem as FornecedorDTO;
            string nome = dto.Nome;

            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoConsultarView> lista = business.ConsultarPorFornecedor(nome);

            cboProduto.ValueMember = nameof(ProdutoConsultarView.Id);
            cboProduto.DisplayMember = nameof(ProdutoConsultarView.Nome);
            cboProduto.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                ProdutoConsultarView dto = cboProduto.SelectedItem as ProdutoConsultarView;
                produtosCarrinho.Remove(dto);

            }
        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
