﻿using Hamburgueria.DB.Compras.Produto;
using Hamburgueria.Telas.Compras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Produto
{
    public partial class ConsultarProduto : Form
    {
        public ConsultarProduto()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        public void CarregarGrid()
        {
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoConsultarView> dto = business.Consultar(txtProduto.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = dto;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar o produto: " + ex.Message, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }


        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                ProdutoConsultarView produto = dgvProdutos.CurrentRow.DataBoundItem as ProdutoConsultarView;

                this.Hide();
                AlterarProduto tela = new AlterarProduto();
                tela.LoadScreen(produto);

                tela.Show();

            }

            if (e.ColumnIndex == 6)
            {
                ProdutoConsultarView musica = dgvProdutos.CurrentRow.DataBoundItem as ProdutoConsultarView;

                DialogResult r = MessageBox.Show("Deseja excluir o produto?", "Hambúrgueria Império",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    EstoqueBusiness business2 = new EstoqueBusiness();
                    business2.Remover(musica.Id);

                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(musica.Id);

                    CarregarGrid();

                }

            }
        }
    }
}
