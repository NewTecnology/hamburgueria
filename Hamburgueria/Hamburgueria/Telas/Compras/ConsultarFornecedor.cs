﻿using Hamburgueria.DB.Compras.Fornecedor;
using Hamburgueria.Telas.Compras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Fornecedor
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
        public void CarregarGrid()
        {
            try
            {
                FornecedorBusiness business = new FornecedorBusiness();
                List<FornecedorDTO> dto = business.Consultar(txtProduto.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = dto;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar o fornecedor: " + ex.Message, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                FornecedorDTO produto = dgvProdutos.CurrentRow.DataBoundItem as FornecedorDTO;

                this.Hide();
                AlterarFornecedor tela = new AlterarFornecedor();
                tela.LoadScreen(produto);

                tela.Show();

            }

            if (e.ColumnIndex == 9)
            {
                FornecedorDTO fornecedor = dgvProdutos.CurrentRow.DataBoundItem as FornecedorDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o fornecedor?", "Hambúrgueria Império",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Remover(fornecedor.Id);
                    CarregarGrid();

                }
            }
        }
    }
}
