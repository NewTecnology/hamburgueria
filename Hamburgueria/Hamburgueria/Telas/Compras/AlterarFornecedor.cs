﻿using Hamburgueria.DB.Compras.Fornecedor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Compras
{
    public partial class AlterarFornecedor : Form
    {
        public AlterarFornecedor()
        {
            InitializeComponent();
        }
        FornecedorDTO fornecedor;
        public void LoadScreen(FornecedorDTO dto)
        {
            this.fornecedor = dto;
            label13.Text = Convert.ToString(dto.Id);
            txtNome.Text = dto.Nome;
            maskedTextBox2.Text = dto.Telefone;
            textBox3.Text = dto.Email;
            maskedTextBox3.Text = dto.CNPJ;
            txtRua.Text = dto.Rua;
            txtCEP.Text = dto.CEP;
            txtBairro.Text = dto.Bairro;
            txtCidade.Text = dto.Cidade;
            txtEstado.Text = dto.Estado;

        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();

                if (txtCEP.MaskCompleted)
                {
                    dto.CEP = txtCEP.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CEP é obrigatório");
                }

                if (maskedTextBox3.MaskCompleted)
                {
                    dto.CNPJ = maskedTextBox3.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CNPJ é obrigatório");
                }

                if (maskedTextBox2.MaskCompleted)
                {
                    dto.Telefone = maskedTextBox2.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("Telefone é obrigatório");
                }

                
                dto.Id = Convert.ToInt32(label13.Text);
                dto.Nome = txtNome.Text.Trim();
                dto.Email = textBox3.Text.Trim();
                dto.Rua = txtRua.Text.Trim();
                dto.Bairro = txtBairro.Text.Trim();
                dto.Cidade = txtCidade.Text.Trim();
                dto.Estado = txtEstado.Text.Trim();

                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(dto);

                EnviarMensagem("Fornecedor alterado com sucesso");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

       
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void maskedTextBox1_Validated(object sender, EventArgs e)
        {
            // Lê e formata o CEP do textbox
            string cep = txtCEP.Text.Trim().Replace("-", "");

            // Chama função BuscarAPICorreio
            CorreioResponde correio = BuscarAPICorreio(cep);

            // Altera os valores dos textbox com a resposta do correio
            txtRua.Text = correio.Logradouro + " - " + correio.Complemento;
            txtBairro.Text = correio.bairro;
            txtCidade.Text = correio.localidade;
            txtEstado.Text = correio.uf;


        }

        private CorreioResponde BuscarAPICorreio(string cep)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Chama API do correio, concatenando o cep
            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            // Transforma a resposta do correio em DTO
            CorreioResponde correio = JsonConvert.DeserializeObject<CorreioResponde>(resposta);
            return correio;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
