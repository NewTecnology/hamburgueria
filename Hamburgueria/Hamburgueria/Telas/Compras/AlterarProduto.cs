﻿using Hamburgueria.DB.Compras.Fornecedor;
using Hamburgueria.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Compras
{
    public partial class AlterarProduto : Form
    {
        public AlterarProduto()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            FornecedorDTO dto = new FornecedorDTO();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;

        }

        ProdutoConsultarView produto;
        public void LoadScreen(ProdutoConsultarView dto)
        {
            this.produto = dto;
            label7.Text = Convert.ToString(dto.Id);
            txtProduto.Text = dto.Nome;
            numericUpDown1.Value = dto.Preco;
            textBox2.Text = dto.Marca;

        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = comboBox1.SelectedItem as FornecedorDTO;
                ProdutoDTO produto2 = new ProdutoDTO();

                produto2.Id = Convert.ToInt32(label7.Text);
                produto2.Nome = txtProduto.Text.Trim();
                produto2.Preco = numericUpDown1.Value;
                produto2.Marca = textBox2.Text.Trim();
                produto2.IdFornecedor = fornecedor.Id;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Alterar(produto2);

                EnviarMensagem("Produto alterado com sucesso.");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = comboBox1.SelectedItem as FornecedorDTO;
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
