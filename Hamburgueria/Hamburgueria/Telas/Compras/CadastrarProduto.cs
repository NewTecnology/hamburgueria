﻿using Hamburgueria.DB.Compras.Estoque;
using Hamburgueria.DB.Compras.Fornecedor;
using Hamburgueria.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Produto
{
    public partial class CadastrarProduto : Form
    {
        BindingList<EstoqueConsultarView> estoque = new BindingList<EstoqueConsultarView>();
        public CadastrarProduto()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            FornecedorDTO dto = new FornecedorDTO();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;


        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = comboBox1.SelectedItem as FornecedorDTO;

                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Preco = numericUpDown1.Value;
                dto.Marca = textBox2.Text.Trim();
                dto.IdFornecedor = fornecedor.Id;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto,estoque.ToList());

                MessageBox.Show("Produto cadastrado com sucesso", "Hambúrgueria Império");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
