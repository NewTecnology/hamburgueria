﻿using Hamburgueria.DB.Compras.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Pedido_de_Compra
{
    public partial class ConsultarPedido : Form
    {
        public ConsultarPedido()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            PedidoBusiness business = new PedidoBusiness();
            List<PedidoCompraConsultarView> lista = business.Consultar(dateTimePicker1.Value, dateTimePicker2.Value);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = lista;

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                PedidoCompraConsultarView pedido = dgvPedidos.CurrentRow.DataBoundItem as PedidoCompraConsultarView;

                DialogResult r = MessageBox.Show("Deseja excluir o pedido?", "Hambúrgueria Império",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    PedidoBusiness business = new PedidoBusiness();
                    business.Remover(pedido.Id);
                    CarregarGrid();

                }

            }
        }
    }
}
