﻿using Hamburgueria.DB.Compras.Fornecedor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Fornecedor
{
    public partial class CadastrarFornecedor : Form
    {
        public CadastrarFornecedor()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

        }

        private void CadastrarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO Fornecedor = new FornecedorDTO();

                if (txtCEP.MaskCompleted)
                {
                    Fornecedor.CEP = txtCEP.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CEP é obrigatório");
                }

                if (maskedTextBox3.MaskCompleted)
                {
                    Fornecedor.CNPJ = maskedTextBox3.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CNPJ é obrigatório");
                }

                if (maskedTextBox2.MaskCompleted)
                {
                    Fornecedor.Telefone = maskedTextBox2.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("Telefone é obrigatório");
                }

                Fornecedor.Cidade = txtCidade.Text.Trim();
                Fornecedor.Email = textBox3.Text.Trim();
                Fornecedor.Estado = txtEstado.Text;
                Fornecedor.Nome = txtProduto.Text.Trim();
                Fornecedor.Rua = txtRua.Text.Trim();
                Fornecedor.Bairro = txtBairro.Text.Trim();

                FornecedorBusiness FornecedorOi = new FornecedorBusiness();
                FornecedorOi.Salvar(Fornecedor);

                EnviarMensagem("Fornecedor cadastrado com sucesso.");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
               
               
            

        }



        private void txtCEP_Validated(object sender, EventArgs e)
        {
            // Lê e formata o CEP do textbox
            string cep = txtCEP.Text.Trim().Replace("-", "");

            // Chama função BuscarAPICorreio
            CorreioResponde correio = BuscarAPICorreio(cep);

            // Altera os valores dos textbox com a resposta do correio
            txtRua.Text = correio.Logradouro + " - " + correio.Complemento;
            txtBairro.Text = correio.bairro;
            txtCidade.Text = correio.localidade;
            txtEstado.Text = correio.uf;
            

        }


        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private CorreioResponde BuscarAPICorreio(string cep)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Chama API do correio, concatenando o cep
            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            // Transforma a resposta do correio em DTO
            CorreioResponde correio = JsonConvert.DeserializeObject<CorreioResponde>(resposta);
            return correio;
        }

        private void txtCEP_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
