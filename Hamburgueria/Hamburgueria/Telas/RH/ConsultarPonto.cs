﻿using Hamburgueria.DB.RH.Ponto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.RH
{
    public partial class ConsultarPonto : Form
    {
        public ConsultarPonto()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                PontoBusiness business = new PontoBusiness();
                List<PontoConsultarView> dto = business.Consultar(txtFuncionario.Text.Trim());


                dgvPontos.AutoGenerateColumns = false;
                dgvPontos.DataSource = dto;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar os pontos " + ex.Message, "Hambúrgueria Império",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

            }
            

        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtFuncionario_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvPontos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
