﻿using Hamburgueria.DB.Base;
using Hamburgueria.DB.Funcionário;
using Hamburgueria.DB.RH.Ponto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.RH
{
    public partial class frmPonto : Form
    {
        public frmPonto()
        {
            InitializeComponent();
            lblDiaAtual.Text = DateTime.Now.ToShortDateString();
            label4.Text = LogandoUsuario.UsuarioLogado.NomeFuncionario;
            label5.Text = Convert.ToString(LogandoUsuario.UsuarioLogado.Id);
        }

        private void label3_Click(object sender, EventArgs e)
        {
           
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        PontoConsultarView view = new PontoConsultarView();

        private void btnSalvar_Click(object sender, EventArgs e)
        {

           if (lblDiaAtual.Text == Convert.ToString(view.Data)  && label4.Text == LogandoUsuario.UsuarioLogado.NomeFuncionario)
           {
               MessageBox.Show("Esse funcionário já cadastrou o ponto");
           }
           else
           {
                int horas = (dateTimePicker4.Value.Hour - dateTimePicker1.Value.Hour);
                int totalhoras = horas;
                PontoDTO ponto = new PontoDTO();
                ponto.Data = Convert.ToDateTime(lblDiaAtual.Text);
                ponto.Entrada = dateTimePicker1.Value;
                ponto.IdaAlmoco = dateTimePicker2.Value;
                ponto.VoltaAlmoco = dateTimePicker3.Value;
                ponto.Saida = dateTimePicker4.Value;
                ponto.HorasTrabalhadasDia = totalhoras;
                ponto.IdFuncionario = Convert.ToInt32(label5.Text);

                PontoBusiness business = new PontoBusiness();
                business.Salvar(ponto);

                EnviarMensagem("Ponto cadastrado com sucesso.");

           }

            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();

        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void dateTimePicker1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

        }

        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
