﻿using Hamburgueria.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Funcionário
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                FuncionarioDTO funcionario = dgvProdutos.CurrentRow.DataBoundItem as FuncionarioDTO;

                this.Hide();
                AlterarFuncionario tela = new AlterarFuncionario();
                tela.LoadScreen(funcionario);

                tela.Show();

            }

            if (e.ColumnIndex == 6)
            {
                FuncionarioDTO func = dgvProdutos.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o funcionário?", "Hambúrgueria Império",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(func.Id);
                    CarregarGrid();

                }

            }

        }


        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();

            
        }


        public void CarregarGrid()
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> dto = business.Consultar(textBox3.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = dto;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar o funcionário: " + ex.Message, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }


    }


}
