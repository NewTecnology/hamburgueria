﻿using Hamburgueria.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Funcionário
{
    public partial class AlterarFuncionario : Form
    {
        public AlterarFuncionario()
        {
            InitializeComponent();
            checkBox1.Enabled = false;
            checkBox2.Checked = true;

        }

        FuncionarioDTO funcionario;

        public void LoadScreen(FuncionarioDTO funcionario)
        {
            this.funcionario = funcionario;
            lblId.Text = funcionario.Id.ToString();
            textBox3.Text = funcionario.NomeFuncionario;
            maskedTextBox2.Text = funcionario.CPF;
            maskedTextBox1.Text = funcionario.RG;
            textBox2.Text = funcionario.Endereco;
            numericUpDown1.Value = funcionario.Salario;
            numericUpDown2.Value = funcionario.ValeTransporte;
            numericUpDown3.Value = funcionario.ValeAlimentacao;
            numericUpDown4.Value = funcionario.ValeRefeicao;
            numericUpDown5.Value = funcionario.Convenio;
            textBox13.Text = funcionario.Usuario;
            txtSenha.Text = funcionario.Senha;
            checkBox2.Checked = funcionario.Funcionario;
            checkBox7.Checked = funcionario.RH;
            checkBox6.Checked = funcionario.Compras;
            checkBox4.Checked = funcionario.Logistica;
            checkBox5.Checked = funcionario.Vendas;
            checkBox3.Checked = funcionario.Financeiro;
        }

        private void button1_Click(object sender, EventArgs e)
        {
           try
           {
                if (maskedTextBox2.MaskCompleted)
                {
                    funcionario.CPF = maskedTextBox2.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CPF é obrigatório");
                }

                if (maskedTextBox1.MaskCompleted)
                {
                    funcionario.RG = maskedTextBox1.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("RG é obrigatório");
                }

                funcionario.NomeFuncionario = textBox3.Text.Trim();
                funcionario.Endereco = textBox2.Text.Trim();
                funcionario.Salario = numericUpDown1.Value;
                funcionario.ValeTransporte = numericUpDown2.Value;
                funcionario.ValeAlimentacao = numericUpDown3.Value;
                funcionario.ValeRefeicao = numericUpDown4.Value;
                funcionario.Convenio = numericUpDown5.Value;
                funcionario.Usuario = textBox13.Text.Trim();
                funcionario.Senha = txtSenha.Text.Trim();
                funcionario.Admin = checkBox1.Checked;
                funcionario.Funcionario = checkBox2.Checked;
                funcionario.RH = checkBox7.Checked;
                funcionario.Compras = checkBox6.Checked;
                funcionario.Logistica = checkBox4.Checked;
                funcionario.Vendas = checkBox5.Checked;
                funcionario.Financeiro = checkBox3.Checked;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Alterar(funcionario);

                EnviarMensagem("Funcionário alterado com sucesso.");
                this.Hide();

                frmLogin tela = new frmLogin();
                tela.Show();
           }
           catch (ArgumentException ex)
           {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
           }


        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

       

        private void label10_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void lblId_Click(object sender, EventArgs e)
        {

        }

        private void AlterarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
           
        }

        private void numericUpDown1_Validated(object sender, EventArgs e)
        {
            decimal resultado = numericUpDown1.Value;
            decimal resultado2 = (resultado * 6) / 100;
            numericUpDown2.Value = resultado2;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
