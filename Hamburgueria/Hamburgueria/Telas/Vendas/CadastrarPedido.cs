﻿using Hamburgueria.DB.Compras.Produto;
using Hamburgueria.DB.Plugin;
using Hamburgueria.DB.Vendas.Cliente;
using Hamburgueria.DB.Vendas.Pedido;
using Hamburgueria.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Pedido_de_Venda
{
    public partial class CadastrarPedido : Form
    {
        BindingList<ProdutoVendaDTO> produtosCarrinho = new BindingList<ProdutoVendaDTO>();
        public CadastrarPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        void CarregarCombos()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(ClienteDTO.Id);
            comboBox1.DisplayMember = nameof(ClienteDTO.Nome);
            comboBox1.DataSource = lista;

            ProdutoVendaBusiness business2 = new ProdutoVendaBusiness();
            List<ProdutoVendaDTO> lista2 = business2.Listar();

            comboBox2.ValueMember = nameof(ProdutoVendaDTO.Id);
            comboBox2.DisplayMember = nameof(ProdutoVendaDTO.Nome);
            comboBox2.DataSource = lista2;
        }

        void ConfigurarGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = produtosCarrinho;
        }
        private void CadastrarPedido_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            
            
            try
            {
                string mensagem = produtosCarrinho.ToString();

                string a = "";
                List<string> nomes = new List<string>();
                string b = string.Empty;
                int quantidade = 1;
                b = produtosCarrinho[0].Nome;
                string c = "";
                foreach (ProdutoVendaDTO item in produtosCarrinho)
                {
                    if (b == item.Nome)
                    {
                        a = quantidade.ToString() + item.Nome;
                        quantidade = quantidade + 1;
                    }
                    if (b != item.Nome)
                    {
                        if (c == "")
                        {
                            quantidade = 1;
                        }
                        c = quantidade.ToString() + item.Nome;
                        quantidade = quantidade + 1;

                    }

                }
                a = a + ", " + c;
                Email email = new Email();
                if(txtEmail.Text != string.Empty)
                {
                    email.Enviar(txtEmail.Text, a);
                }


                ClienteDTO dto2 = comboBox1.SelectedItem as ClienteDTO;

                PedidoVendaDTO dto = new PedidoVendaDTO();
                dto.IdCliente = dto2.Id;
                dto.Data = DateTime.Now;

                PedidoVendaBusiness business = new PedidoVendaBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido Salvo com sucesso", "Hambúrgueria Império", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao cadastrar o pedido: " + ex.Message, "Hambúrgueria Império",
                      MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoVendaDTO dto = comboBox2.SelectedItem as ProdutoVendaDTO;

            int qtd = Convert.ToInt32(numericUpDown1.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);
            }
        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
          // string emailPara = txtEmail.Text;
          //string mensagem = lblMessage.Text;

          //  DB.Plugin.Email email = new DB.Plugin.Email();
          //email.Enviar(emailPara, mensagem);

          //txtEmail.Visible = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO cliente = comboBox1.SelectedItem as ClienteDTO;
            txtEmail.Text = cliente.Email;
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
