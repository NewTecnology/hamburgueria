﻿using Hamburgueria.DB.Compras.Fornecedor;
using Hamburgueria.DB.Vendas.Cliente;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Vendas
{
    public partial class AlterarCliente : Form
    {
        public AlterarCliente()
        {
            InitializeComponent();
        }
        ClienteDTO cliente;
        public void LoadScreen(ClienteDTO dto)
        {
            this.cliente = dto;
            label2.Text = Convert.ToString(dto.Id);
            radioButton1.Checked = dto.PessoaFisica;
            radioButton2.Checked = dto.PessoaJuridica;
            textBox3.Text = dto.Nome;
            maskedTextBox1.Text = dto.RG;
            maskedTextBox2.Text = dto.CPF;
            maskedTextBox5.Text = dto.CNPJ;
            maskedTextBox3.Text = dto.Telefone;
            maskedTextBox4.Text = dto.Celular;
            textBox1.Text = dto.Email;
            txtRua.Text = dto.Rua;
            txtCEP.Text = dto.CEP;
            txtBairro.Text = dto.Bairro;
            txtCidade.Text = dto.Cidade;
            txtEstado.Text = dto.Estado;
            textBox8.Text = dto.Descricao;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cadascliente = new ClienteDTO();

                if (maskedTextBox1.MaskCompleted)
                {
                    cadascliente.RG = maskedTextBox1.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("RG é obrigatório");
                }

                if (maskedTextBox2.Enabled == true)
                {
                    if (maskedTextBox2.MaskCompleted)
                    {
                        cadascliente.CPF = maskedTextBox2.Text.Trim();
                        cadascliente.CNPJ = maskedTextBox5.Text;

                    }

                    else
                    {
                        throw new ArgumentException("CPF é obrigatório");
                    }

                }

                if (maskedTextBox5.Enabled == true)
                {
                    if (maskedTextBox5.MaskCompleted)
                    {
                        cadascliente.CNPJ = maskedTextBox5.Text.Trim();
                        cadascliente.CPF = maskedTextBox2.Text;
                    }

                    else
                    {
                        throw new ArgumentException("CNPJ é obrigatório");
                    }

                }

                cadascliente.Id = Convert.ToInt32(label2.Text);
                cadascliente.Nome = textBox3.Text.Trim();
                cadascliente.DataCadastro = DateTime.Now;
                cadascliente.Telefone = maskedTextBox3.Text.Trim();
                cadascliente.Celular = maskedTextBox4.Text.Trim();
                cadascliente.Email = textBox1.Text.Trim();
                cadascliente.Rua = txtRua.Text.Trim();
                cadascliente.CEP = txtCEP.Text.Trim();
                cadascliente.Bairro = txtBairro.Text.Trim();
                cadascliente.Cidade = txtCidade.Text.Trim();
                cadascliente.Estado = txtEstado.Text.Trim();
                cadascliente.Descricao = textBox8.Text.Trim();
                cadascliente.PessoaFisica = radioButton1.Checked;
                cadascliente.PessoaJuridica = radioButton2.Checked;

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(cadascliente);

                EnviarMensagem("Cliente alterado com sucesso");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                  MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBox2.Enabled = true;
            maskedTextBox5.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBox2.Enabled = false;
            maskedTextBox5.Enabled = true;
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void maskedTextBox7_Validated(object sender, EventArgs e)
        {
            // Lê e formata o CEP do textbox
            string cep = txtCEP.Text.Trim().Replace("-", "");

            // Chama função BuscarAPICorreio
            CorreioResponde correio = BuscarAPICorreio(cep);

            // Altera os valores dos textbox com a resposta do correio
            txtRua.Text = correio.Logradouro + " - " + correio.Complemento;
            txtBairro.Text = correio.bairro;
            txtCidade.Text = correio.localidade;
            txtEstado.Text = correio.uf;

        }

        private CorreioResponde BuscarAPICorreio(string cep)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Chama API do correio, concatenando o cep
            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            // Transforma a resposta do correio em DTO
            CorreioResponde correio = JsonConvert.DeserializeObject<CorreioResponde>(resposta);
            return correio;
        }
    }
}
