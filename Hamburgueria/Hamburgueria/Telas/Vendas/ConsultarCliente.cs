﻿using Hamburgueria.DB.Vendas.Cliente;
using Hamburgueria.Telas.Vendas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Cliente
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
        public void CarregarGrid()
        {
           try
           {
                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> dto = business.Consultar(textBox3.Text.Trim());

                dgvClientes.AutoGenerateColumns = false;
                dgvClientes.DataSource = dto;

           }
           catch (Exception ex)
           {
                MessageBox.Show("Ocorreu um erro ao consultar o cliente: " + ex.Message, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

           }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                ClienteDTO cliente = dgvClientes.CurrentRow.DataBoundItem as ClienteDTO;

                this.Hide();
                AlterarCliente tela = new AlterarCliente();
                tela.LoadScreen(cliente);

                tela.Show();

            }

            if (e.ColumnIndex == 10)
            {
                ClienteDTO cliente = dgvClientes.CurrentRow.DataBoundItem as ClienteDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cliente?", "Hambúrgueria Império",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(cliente.Id);
                    CarregarGrid();

                }

            }
        }
    }
}
