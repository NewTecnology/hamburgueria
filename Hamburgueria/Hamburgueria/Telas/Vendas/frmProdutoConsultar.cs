﻿using Hamburgueria.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Vendas
{
    public partial class frmProdutoConsultar : Form
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }
        public void CarregarGrid()
        {
            ProdutoVendaBusiness Business = new ProdutoVendaBusiness();
            List<ProdutoVendaDTO> dto = Business.Consultar(txtProduto.Text);

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = dto;

        }
        private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                ProdutoVendaDTO produto = dgvProdutos.CurrentRow.DataBoundItem as ProdutoVendaDTO;

                this.Hide();
                AlterarProduto tela = new AlterarProduto();
                tela.LoadScreen(produto);

                tela.Show();

            }

            if (e.ColumnIndex == 5)
            {
                ProdutoVendaDTO dto = dgvProdutos.CurrentRow.DataBoundItem as ProdutoVendaDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o produto?", "Hambúrgueria Império",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoVendaBusiness business = new ProdutoVendaBusiness();
                    business.Remover(dto.Id);
                    CarregarGrid();

                }

            }
        }

    }


}
