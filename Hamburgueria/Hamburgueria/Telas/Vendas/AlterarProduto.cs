﻿using Hamburgueria.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Vendas
{
    public partial class AlterarProduto : Form
    {
        public AlterarProduto()
        {
            InitializeComponent();
          
        }

        ProdutoVendaDTO produto;
      
        public void LoadScreen(ProdutoVendaDTO dto1)
        {
            this.produto = dto1;
            label7.Text = Convert.ToString(dto1.Id);
            txtProduto.Text = dto1.Nome;
            numericUpDown1.Value = dto1.Preco;

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.Id = Convert.ToInt32(label7.Text);
                dto.Nome = txtProduto.Text.Trim();
                dto.Categoria = comboBox1.Text;
                dto.Preco = numericUpDown1.Value;

                ProdutoVendaBusiness business = new ProdutoVendaBusiness();
                business.Alterar(dto);

                EnviarMensagem("Produto alterado com sucesso.");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
              MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }


        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }

}
