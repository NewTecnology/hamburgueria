﻿using Hamburgueria.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Vendas
{
    public partial class frmProduto : Form
    {
        public frmProduto()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Preco = numericUpDown1.Value;
                dto.Categoria = comboBox1.Text;

                ProdutoVendaBusiness businees = new ProdutoVendaBusiness();
                businees.Salvar(dto);

                EnviarMensagem("Produto cadastrado com sucesso.");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
              MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Hambúrgueria Império",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
