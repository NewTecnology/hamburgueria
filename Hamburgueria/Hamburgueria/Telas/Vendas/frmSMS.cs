﻿using Hamburgueria.DB.Diferenciais.SMS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Vendas
{
    public partial class frmSMS : Form
    {
        public frmSMS()
        {
            InitializeComponent();
        }

        private void btnEnviarSms_Click(object sender, EventArgs e)
        {
            TwilioSMS sms = new TwilioSMS();
            sms.Enviar(txtPara.Text, txtMensagem.Text);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
