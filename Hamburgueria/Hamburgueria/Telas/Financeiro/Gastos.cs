﻿using Hamburgueria.DB.Financeiro.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Financeiro
{
    public partial class Gastos : Form
    {
        public Gastos()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                GastosDTO dto = new GastosDTO();
                dto.Nome = textBox1.Text.Trim();
                dto.Data = dateTimePicker1.Value;
                dto.Valor = numericUpDown1.Value;
                dto.Tipo = comboBox1.Text;

                GastosBusiness business = new GastosBusiness();
                business.Salvar(dto);

                MessageBox.Show("Gasto cadastrado com sucesso", "Hambúrgueria Império");

                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Hambúrgueria Império",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
