﻿using Hamburgueria.DB.Financeiro.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Financeiro
{
    public partial class FluxoDeCaixa : Form
    {
        public FluxoDeCaixa()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxodeCaixaDTO> lista = business.Consultar(dateTimePicker1.Value, dateTimePicker2.Value);

            dgvFluxo.AutoGenerateColumns = false;
            dgvFluxo.DataSource = lista;

        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void dgvFluxo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
        }
    }
}
