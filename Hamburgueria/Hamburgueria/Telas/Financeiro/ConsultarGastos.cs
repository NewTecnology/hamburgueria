﻿using Hamburgueria.DB.Financeiro.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Financeiro
{
    public partial class ConsultarGastos : Form
    {
        public ConsultarGastos()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            GastosBusiness business = new GastosBusiness();
            List<GastosDTO> lista = business.Consultar(dateTimePicker1.Value, dateTimePicker2.Value);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = lista;
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
