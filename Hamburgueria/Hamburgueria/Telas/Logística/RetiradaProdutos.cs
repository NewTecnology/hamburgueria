﻿using Hamburgueria.DB.Compras.Estoque;
using Hamburgueria.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Logística
{
    public partial class RetiradaProdutos : Form
    {
        public RetiradaProdutos()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            EstoqueConsultarView view = new EstoqueConsultarView();

            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueConsultarView> lista = business.Listar();

            comboBox1.ValueMember = nameof(EstoqueConsultarView.Id);
            comboBox1.DisplayMember = nameof(EstoqueConsultarView.Produto);
            comboBox1.DataSource = lista;

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void RetiradaProdutos_Load(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
          
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            EstoqueConsultarView dto = comboBox1.SelectedItem as EstoqueConsultarView;
            numericUpDown1.Value = dto.Quantidade;

        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EstoqueConsultarView dto = comboBox1.SelectedItem as EstoqueConsultarView;
            int IdProduto = dto.Id;

            if (numericUpDown1.Value <= dto.Quantidade && numericUpDown1.Value >= 1)
            {
               ProdutoBusiness business = new ProdutoBusiness();
               EstoqueBusiness business2 = new EstoqueBusiness();
               List<ProdutoConsultarView> lista = business.ConsultarPorId(IdProduto);
               List<EstoqueDTO> estoque = business2.Listar2();

                foreach (ProdutoConsultarView item in lista)
                {
                    foreach (EstoqueDTO item2 in estoque)
                    {
                        if (item.Id == item2.IdProduto)
                        {
                            EstoqueDTO estoquedto = new EstoqueDTO();
                            estoquedto.Id = item.Id;
                            estoquedto.IdProduto = item.Id;
                            estoquedto.Quantidade = item2.Quantidade - Convert.ToInt32(numericUpDown1.Value);

                            business2.Alterar(estoquedto);
                        }

                    }
                }

                

                MessageBox.Show("Produto retirado com sucesso", "Hambúrgueria Império");
                this.Hide();
                frmLogin tela = new frmLogin();
                tela.Show();
            }
            else if(numericUpDown1.Value == 0)
            {
                MessageBox.Show("A quantidade deve ser maior que  0", "Hambúrgueria Império");
            }
            else
            {
               MessageBox.Show("A quantidade selecionada é maior do que a disponível no estoque","Hambúrgueria Império");
            }
        }
    }
}
