﻿using Hamburgueria.DB.Compras.Estoque;
using Hamburgueria.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamburgueria.Telas.Estoque
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();

        }
        void CarregarGrid()
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueConsultarView> lista = business.Consultar(textBox1.Text.Trim());

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }
        private void Estoque_Load(object sender, EventArgs e)
        {


        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
